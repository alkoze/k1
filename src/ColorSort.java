public class ColorSort {

   enum Color {red, green, blue};

   public static void main (String[] param) {
      Color[] arr = {Color.red, Color.red, Color.blue, Color.red, Color.green};
      reorder(arr);
      for (Color color:
              arr) {
         System.out.println(color);
      }
   }

   public static void reorder (Color[] balls) {
      // TODO!!! Your program here
      Color[] copy = balls.clone();
      int firstElement = 0;
      int lastElement = balls.length - 1;
      // Loop goes through the copy of the original array and changes elements in the original array.
      // The begin of the original array is being changed to red elements and the end is changed to blue elements.
      for (Color color : copy) {
         switch (color){
            case red:
               balls[firstElement] = color;
               firstElement += 1;
               break;
            case blue :
               balls[lastElement] = color;
               lastElement -= 1;
               break;
         }
      }
      for (int i = firstElement; i <= lastElement; i++) {
         balls[i] = Color.green;
      }
   }

}